package main

import (
	"testing"
	"database/sql"
	"fmt"
	"net/http"
	"io/ioutil"
	"net/url"
)

func TestOpenDB(t *testing.T) {
	db, err := sql.Open("mysql", "root:315@tcp(127.0.0.1:3306)/test")
	defer db.Close()
	err = db.Ping()
	if err != nil {
	t.Error("Expected ok, got ", err.Error())
	}
}

func TestCRUDcreate(t *testing.T) {

	resp, err := http.PostForm("http://localhost:3000/person",
		url.Values{"first_name": {"Vasya"}, "last_name": {"Pupkin"}})
	if err != nil {/* handle error */ }
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	fmt.Print(string(body))
}

func TestCRUDread(t *testing.T) {

	resp, err := http.Get("http://localhost:3000/persons")
	if err != nil {}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("error:", err)
	}
	fmt.Println(string(body))
}

func TestCRUDdelete(t *testing.T) {


}

/*
func TestCRUDreadnew(t *testing.T) {

	resp, err := http.Get("http://localhost:3000/persons")
	if err != nil {}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	type Out struct {
			count int
			result  string
		}
	var out []Out
	esrr := json.Unmarshal(body, &out)
	if esrr != nil {
		fmt.Println("error:", esrr)
	}
	fmt.Println(out)
	}

func TestTry(t *testing.T) {
	type Response1 struct {
		Page   int
		Fruits []string
	}
	type Response2 struct {
		Page   int      `json:"page"`
		Fruits []string `json:"fruits"`
	}

	type Myjson []map[string]string

	resp, err := http.Get("http://localhost:3000/persons")
	if err != nil {}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {fmt.Println("error:", err)}
	fmt.Println(string(body))

	//byt := []byte(`{"num":6.13,"strs":["a","b"]}`)
	byt := []byte(`{"count":2,"result":[{"Id":1,"First_Name":"test","Last_Name":"testlast"},{"Id":35,"First_Name":"qqq","Last_Name":"www"}]}`)

	var dat map[string]interface{}

	if err := json.Unmarshal(byt, &dat); err != nil {panic(err)}
	fmt.Println(dat)

	count := dat["count"]
	fmt.Println("count:", count)

	resulta := dat["result"]
	fmt.Println("result:", resulta)

	str := `{"page": 1, "fruits": ["apple", "peach"]}`
	res := Response2{}
	json.Unmarshal([]byte(str), &res)
	fmt.Println(res)
	fmt.Println(res.Fruits[0])
	enc := json.NewEncoder(os.Stdout)
	d := map[string]int{"apple": 5, "lettuce": 7}
	enc.Encode(d)
}


func TestTry2(t *testing.T) {
	var jsonBlob = []byte(`[{"Name": "Platypus", "Order": "Monotremata"},
				{"Name": "Quoll","Order": "Dasyuromorphia"}]`)
	type Animal struct {
		Name  string
		Order string
	}
	var animals []Animal
	err := json.Unmarshal(jsonBlob, &animals)
	if err != nil {
		fmt.Println("error:", err)
	}
	fmt.Printf("%+v", animals)
}
*/
